Cypress open
node_modules\.bin\cypress open  

Run tests in headless mode (electron)
node_modules\.bin\cypress run

Run tests with in chrome 
node_modules\.bin\cypress run -b chrome

Run tests in headless mode(electron)
npx cypress run 

Run tests via script (need to add script into package.json)
npm run cy:run

Run test and generate mochawsome reports
cypress run --reporter mochawesome

Run test and store results in Dashboard (key could be found in Test Runner/Settings/Record Key)
node_modules\.bin\cypress run --record --key<your key>

Instructions to run and configure Mochawesome reporting
(https://medium.com/egnyte-engineering/3-steps-to-awesome-test-reports-with-cypress-f4fe915bc246)

Run separate test/spec file 
node_modules\.bin\cypress run --spec cypress/integration/examples/Test8Framework.js --headed

Run tests in docker container using docker-compose.yaml
docker-compose -f docker-compose.yaml up