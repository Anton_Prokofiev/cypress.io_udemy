// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
import 'cypress-file-upload';


// -- This is a parent command --
Cypress.Commands.add("selectProduct", (productName) => {
  cy.get('h4.card-title').each((el, index, $list) => {
    if (el.text().includes(productName)) {
      cy.get('button.btn.btn-info').eq(index).click()
    }
  })
})


Cypress.Commands.add('setSessionStorage', (key, value) => {
  //Turn off logging of the cy.window() to command log
  cy.window({ log: false}).then((window) => {
    window.sessionStorage.setItem(key, value)
  })


  //Start listening "log:added" event
  //After log was added 
  //Printing 'Its working' text in console
  cy.on("log:added", () =>{
    console.log('Its working')
  })

  // debugger
  // cy.on('test:after:run', ()=>{
  //   console.log("after run")
  // })

    const log = Cypress.log({
      name: 'setSessionStorage',
      // shorter name for the Command Log
      displayName: 'setSS',
      message: `${key}, ${value}`,
      consoleProps: () => {
        // return an object which will
        // print to dev tools console on click
        return {
          'Key': key,
          'Value': value,
          'Session Storage': window.sessionStorage
        }
      }
    })
})

Cypress.Commands.add('login', (username, password) => {

  //Long way of working with Promise
  // cy.get('#loginLink').then(($link)=>{
  // return $link.text();
  // }).as('linkText');

  //Shorthand way of working with Promise
  cy.get('#loginLink').invoke('text').as('linkText')

  cy.get('@linkText').then(($x) => {
    expect($x).is.equal('Login');
  })
  //Perform Login click
  cy.contains('Login').click()

  cy.url().should("include", "/Account/Login")

  //Enter username and password
  cy.get('#UserName').type(username);
  cy.get("#Password").type(password);

  cy.get(".btn").click({ force: true })
})

//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
