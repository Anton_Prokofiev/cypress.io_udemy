class HomePage
{

getEditBox()
{
    return cy.get('input[name="name"]:nth-child(2)')
}

getFieldData()
{
    return cy.get(':nth-child(4) > .ng-pristine')
}

getGender()
{
    return cy.get('select')
}

getRadioBtn()
{
    return cy.get('#inlineRadio3')
}

getShopTab()
{
    return cy.get(':nth-child(2) > .nav-link')
}

getEditBoxByXpath()
{
    return cy.xpath("//input[contains(@class, 'form-control') and @name='name']")
}

clickOnCheckoutBtn()
{
    cy.get('#navbarResponsive > .navbar-nav > .nav-item > .nav-link').click()
}

}

export default HomePage;