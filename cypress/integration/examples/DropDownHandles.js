/// <reference types="Cypress" />


describe('My First Test', function() {
    it('DropDownHandles', function() {
    cy.visit("https://rahulshettyacademy.com/AutomationPractice/")

    //chechboxes
    cy.get("#checkBoxOption1").check().should('be.checked').and('have.value', 'option1')
    cy.get("#checkBoxOption1").uncheck().should('not.be.checked')
    cy.get('input[type="checkbox"]').check(['option2','option3'])


    //static dropdown
    cy.get('select').select('option2').should('have.value', 'option2')
    
    //Dynamic dropdown
    cy.get('#autocomplete').type('Uk')
    cy.get(".ui-menu-item div").each(($el) => {

      if($el.text()==="Ukraine")
      {
        $el.click()
      }
    })
    cy.get('#autocomplete').should('have.value', 'Ukraine');
   
    Cypress.$.getJSON("https://jsonplaceholder.typicode.com/posts", (data)=>{
        console.log('data', data[1].title)   
        
    })
  })
})