/// <reference types="Cypress" />
import HomePage from '../../support/pageObjects/HomePage'
import ProductPage from '../../support/pageObjects/ProductPage'
import commonBeforeEach2 from '../../support/helpers'

//beforeEach(commonBeforeEach2)
describe('My 9th test',()=>{
    const homepage = new HomePage()

    let data = null;
    before(()=>{
        cy.fixture('example').then(d =>
        {
             data = d
        })
        cy.viewport(1600, 2000)
    })

    it('Test Framework',()=> {
        cy.visit(Cypress.env("url")+"/angularpractice/")
        homepage.getEditBoxByXpath().type(data.name)
        homepage.getGender().select(data.gender)
        homepage.getFieldData().should('have.value', data.name)
        homepage.getEditBox().should('have.attr', 'minlength', '2')
        homepage.getRadioBtn().should('be.disabled')
        homepage.getShopTab().click()

        data.productName.forEach(element => {
            cy.selectProduct(element) 
        });

        homepage.clickOnCheckoutBtn()
        var sum=0
        cy.get('tr td:nth-child(4) strong').each((el, index, list) =>{
            const amount = el.text()
            //cy.log(el.text())
            var res = amount.split(" ")
            res=res[1].trim()
            sum=Number(sum)+Number(res)
        }).then(()=>{
            cy.log(sum)
        })
        cy.get('h3 strong').then(el=>{
            const amount = el.text()
            var total = amount.split(" ")
            total=total[1].trim()
            expect(Number(sum)).to.equal(Number(total))
        })
        cy.contains('Checkout').click()
        cy.get('#country').type('India')
        cy.get('.suggestions > ul > li > a').click()
        cy.get('#checkbox2').click({force:true})
        cy.get("input[type='submit']").click()
        cy.get('.alert').then(element=>{
            const actualText = element.text()
            expect(actualText.includes("Success!")).to.be.true
        })
    })
})