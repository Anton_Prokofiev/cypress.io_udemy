/// <reference types="Cypress" />


describe('My 11th test', function() {
    it('WindowsAllerts', function() {
   // cy.visit("https://rahulshettyacademy.com/AutomationPractice/")
    cy.visit(Cypress.env("url")+"/AutomationPractice/")
    
    //Cypress auto accept windows alerts
    cy.get('#alertbtn').click()
    cy.get("[value='Confirm']").click()

    //Take over control on Window alerts
    cy.on('window:alert', (str)=>
    {
        //Mocha
        expect(str).to.equal('Hello , share this practice page and share your knowledge')
    })
    cy.on('window:confirm', (str)=>
    {
        //Mocha
        expect(str).to.equal('Hello , Are you sure you want to confirm?')
    })


  })
})