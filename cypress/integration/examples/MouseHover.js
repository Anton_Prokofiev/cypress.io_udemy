/// <reference types="Cypress" />


describe('My 5th test', function() {
    it('MouseHover', function() {
      cy.visit("https://rahulshettyacademy.com/AutomationPractice/")
      
      //click on hidden element
      cy.contains('Top').click({force: true})
      cy.url().should('include', 'top')

      //mouse hover on drop down menu and click
      cy.get('div.mouse-hover-content').invoke('show')
      cy.contains('Top').click()
      cy.url().should('include', 'top')
  

  })
})