/// <reference types="Cypress" />


describe('My 6th test', function() {
    it('New tab handling', function() {
    cy.visit("http://qaclickacademy.com/practice.php")
    
    cy.get('#opentab').invoke('removeAttr','target').click()
    
    cy.url().should('include', 'rahulshettyacademy')

    cy.go('back')
    


  })
})