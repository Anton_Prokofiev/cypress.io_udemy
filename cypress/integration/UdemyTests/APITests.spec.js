/// <reference types="Cypress"/>

describe('Test API from the Fake JSON Server', () => {

    beforeEach("DELETE before creating a new value", () => {

        cy.request({
            method: 'DELETE',
            url: 'http://localhost:3000/posts/3',
            failOnStatusCode: false

        }).then((x) => {
            expect(x.body).to.be.empty
        })
    })

    it('Test GET functionality of JSON Server', () => {
        cy.request('http://localhost:3000/posts/2').its('body').should('have.property', 'id');

    })
    it.only('Test POST functionality of JSON Server', () => {
        cy.request({
            method: 'POST',
            url: 'http://localhost:3000/posts/',
            body: {
                'id': 3,
                "title": "Executeautomation",
                "author": "AntonProk"
            }
        }).then((resp) => {
            expect(resp.body).has.property('title', 'Executeautomation')
        })
    })
})