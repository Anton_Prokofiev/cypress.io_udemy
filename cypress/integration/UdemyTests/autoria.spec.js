/// <reference types="Cypress"/>

describe('test', () => {
    it.only('full_search', () => {
        cy.visit('https://auto.ria.com/')
        // cy.get('#categories').select("Легковые")
        // cy.get('#brandTooltipBrandAutocomplete-brand').type("Mercedes-Benz")
        // cy.get('#brandTooltipBrandAutocomplete-brand > .unstyle.scrollbar.autocomplete-select').contains("Mercedes-Benz").click()
        // cy.get('[id="brandTooltipBrandAutocompleteInput-model"]').type("C-Class", {force: true}).click()
        // cy.get('#brandTooltipBrandAutocomplete-model > .unstyle.scrollbar.autocomplete-select').contains("C-Class").click()
        cy.get(".footer-form >a.ext-end").click({ force: true })
        cy.get('[for="verifiedVIN"]').click()
        cy.get('[id="body.id[2]"]').click({ force: true })
        cy.get('[id="body.id[6]"]').click({ force: true })
        cy.get('[for="more-body-type"]').click()
        cy.get('[id="body.id[254]"]').click({ force: true })
        //cy.get('#at_country').select('Германия')
        //cy.get('#at_country').find('option[value="276"]').eq(0).click({force:true})

        cy.get('#at_country').then(($select) => {
            const option = $select.find('option[value="276"]')
            $select.val(option.attr('value'))
        
            const text = Cypress.$('#at_country option').filter(':selected').text();
            expect(text).is.eql('Германия')
        })
    })
})