/// <reference types="Cypress"/>

context('Actions', ()=>{
    beforeEach(()=>{
        cy.visit("https://fineuploader.com/demos.html")
    })

    it("file uploud demo", ()=>{
        
        cy.fixture("CyDebug.png", 'base64').then(fileContent =>{
            cy.get('.buttons > .qq-upload-button-selector > input').attachFile({
                fileContent,
                fileName : 'CyDebugTest.png',
                mimeType: 'image/png'
            })
            // {
            //     uploadType: 'input'
            // })
        })
    })
})