/// <reference types="Cypress"/>

describe("Testing of EA App", () => {
    before('Login ti application', ()=>{
        //Visiting website
        cy.visit('/');
        cy.fixture("eauser").as("user");

        cy.get("@user").then((user) => {
            cy.login(user.Admin.UserName, user.Admin.Password);
        })
       
        // PreserveOnce is saving cookies for further sessions 
        // let cookie = Cypress.Cookies.preserveOnce('.AspNet.ApplicationCookie')

        //Get cookies value
        cy.getCookie('.AspNet.ApplicationCookie').then((cookie)=>{
            console.log(cookie.value)
        })

    })
    it("Performing Benefit check ", () => {


        cy.contains("Employee List").click()

        //cy.get('.table').find('tr').contains('Karthik Kumar').parent().contains('Benefits').click()

        cy.get('.table').find('tr > td').then(($td)=>{
            cy.wrap($td).contains('Karthik Kumar').parent().contains('Benefits').click()
        })
    })
})