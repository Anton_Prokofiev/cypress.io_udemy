/// <reference types="Cypress"/>

describe("XHR Tests", () => {
    beforeEach("Navigate to LambdaTest", () => {
        cy.visit("https://accounts.lambdatest.com/login")
    })
    it("Perform login and verify XHR ", () => {

        //Start the server
        cy.server()

        cy.route({
            method:"GET",
            url: '/api/user/organization/team'
        }).as('team');

        cy.route({
            method:"GET",
            url: '/api/user/organization/automation-test-summary'
        }).as('apicheck');

        cy.fixture("lambdauser").as('lambdauser');

        cy.get("@lambdauser").then((lambdauser) => {
            cy.get("[name='email']").debug().type(lambdauser.UserName);
            cy.get("[name='password']").debug().type(lambdauser.Password, { log: false });
        })

        cy.get("[class='btn btn-dark submit-btn']").click()

        cy.get('@team').then((xhr)=>{
            expect(xhr.status).to.eq(200);
            expect(xhr.response.body.data[0]).to.have.property('name', 'Anton Prokofiev')
        })

        //Explicit assertion
        cy.get('@apicheck').then((xhr)=>{
            expect(xhr.status).to.eq(200);
            expect(xhr.response.body).to.have.property('maxQueue', 10)
        });
        //Implicit assertion
        cy.get('@apicheck').its('response.body').should('have.property', 'maxQueue').and('eql', 10)
    })
    it('Verify LambdaTest cookies', () =>{

        cy.fixture("lambdauser").as('lambdauser');

        cy.get("@lambdauser").then((lambdauser) => {
            cy.get("[name='email']").debug().type(lambdauser.UserName);
            cy.get("[name='password']").debug().type(lambdauser.Password, { log: false });
        })

        cy.get("[class='btn btn-dark submit-btn']").click()

        cy.getCookie('user_id').should('have.property', 'value', '170705');
    })
})