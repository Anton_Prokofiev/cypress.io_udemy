/// <reference types="Cypress" />

import { resolve } from "cypress/types/bluebird"

describe('My 7th test', function () {
  it('Test1', function () {
    cy.visit("https://rahulshettyacademy.com/seleniumPractise/#/")
    cy.get(".search-keyword").type("ca")
    cy.wait(2000)
    cy.get(".product:visible").should('have.length', 4)
    cy.get(".products").find(".product").eq(2).contains("ADD TO CART").click()

    cy.get(".products").find(".product").each(($el) => {

      const textVeg = $el.find('h4.product-name').text()
      if (textVeg.includes('Cashews')) {
        $el.find('button').click()
      }
    })
  })
})

