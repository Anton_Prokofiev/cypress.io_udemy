FROM cypress/base:12.6.0
WORKDIR /app

COPY package.json .
COPY package-lock.json .
COPY reporter-config.json .

RUN npm install --save-dev cypress

RUN $(npm bin)/cypress verify

COPY cypress cypress
COPY cypress.json .

RUN $(npm bin)/cypress